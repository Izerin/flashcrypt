# Flashcrypt is a simple command-line tool for encrypting and decrypting files
# using the Fernet symmetric encryption algorithm provided by the cryptography package.
# The tool demonstrates the potential functionality of generating and storing a new
# encryption key on an ESP32-S2 microcontroller via the esptool package.

# Note that the script does not include any error handling beyond printing
# warning and error messages. The functions may raise exceptions if input
# parameters are invalid or if external dependencies (e.g., the cryptography
# or esptool packages) are not installed or functioning correctly.

from cryptography.fernet import Fernet
import esptool
import tkinter as tk;
from tkinter import filedialog;
import os

# A simple function that prints a welcome message with the provided name parameter.
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Welcome to {name}')  # Press Ctrl+F8 to toggle the breakpoint.

# A function that prints a list of menu options for the user to select from.
# These options include selecting a file to encrypt/decrypt,
# locking and unlocking files, generating a new key,
# printing the menu options again, and quitting the program.
def printMenuOptions():
    print("f:\tChoose a File")
    print("l:\tLock")
    print("u:\tUnlock")
    print("k:\tGenerate a New Key")
    print("m:\tChange Keyfile Storage Method")
    print("h:\tPrint these options")
    print("q:\tQuit")

# A function that prints a confirmation message to the user
# and prompts for a "yes" or "no" response. The function
# returns True if the user responds with "y" or nothing
# (i.e., just pressing Enter), and False if the user
# responds with "n".
def confirm(message):
    print(message);
    validInput = False;
    while(not validInput):
        capture=input("[Y/n]");
        if(capture == '' or capture.lower() == "y"):
            validInput = True;
            return True;
        elif(capture.lower() == "n"):
            validInput = True;
            return False;
        else:
            pass;

# The removeFile function is designed to remove a file specified
# by the filename parameter from the filesystem.
# If the file specified by the filename parameter does not exist,
# the function prints a message indicating that the file does not exist.
def removeFile(filename):
    if(os.path.exists(filename)):
        try:
            os.remove(filename);
        except:
            print("Unable to remove file %s" % filename);
    else:
        print("%s does not exist." % filename)


# A function that prompts the user to select a file
# using a file dialog provided by the tkinter package.
# The function returns the path to the selected file, or
# forces the user to try again if there is an error reading
# the file.
def openFile():
    valid = False;
    while(not valid):
        root=tk.Tk();
        root.withdraw();

        file_path = filedialog.askopenfilename();

        if(os.path.exists(file_path)):
            print("Current File: %s" % file_path)
            return file_path;
        else:
            print("Filename is invalid.");

# A function that encrypts the contents of a file using
# the Fernet encryption algorithm with a key obtained
# via the scan() function. The encrypted data is used to
# overwrite the selected file. If the file does not exist,
# a warning message is printed.
def lock(file, external):
    if(not external):
        try:
            with open('keyfile.bin', 'rb') as keyfile:
                key = keyfile.read();
        except:
            print("[WARN][LOCAL] Ruh roh, Raggy! The local key doesn't exist!");
    else:
        key = scan();

    fernet = Fernet(key);
    try:
        with open(file, 'rb') as original:
            dataIn = original.read();

        locked = fernet.encrypt(dataIn);

        with open(file, 'wb') as encrypted:
            encrypted.write(locked);
    except:
        print("[WARN] Ruh roh, Raggy! That file doesn't exist!");

# A function that decrypts the contents of a file
# using the Fernet encryption algorithm with a key
# obtained from the scan() function. The decrypted
# data is written back to the same file. If the file
# is already unlocked or the key cannot be obtained,
# an appropriate message is printed.
def unlock(file, external):
    if (not external):
        try:
            with open('keyfile.bin', 'rb') as keyfile:
                key = keyfile.read();
        except:
            print("[WARN][LOCAL] Ruh roh, Raggy! The local key doesn't exist!");
    else:
        key = scan();

    try:
        fernet = Fernet(key);
        with open(file, 'rb') as encrypted:
            dataIn = encrypted.read();

        try:
            unlocked = fernet.decrypt(dataIn);
            with open(file, 'wb') as output:
                output.write(unlocked);
        except:
            print("That file is already unlocked.");
    except:
        print("Invalid/Unable to obtain key!");



#A function that reads a key file from an ESP32-S2 microcontroller
# using the esptool package. The key file is assumed to be located
# at memory address 0x8000 and have a length of 0x1000 bytes.
# The function returns the contents of the key file, or an error
# message if the key file cannot be read.
def scan():
    try:
        args = ['--port', 'COM3', '-b', '460800', 'read_flash', '0x8000', '0x9000', 'flash.bin'];
        esptool.main(args);
        with open('flash.bin', 'rb') as keyfile:
            key = keyfile.read();
            keyfile.close();
            removeFile('flash.bin');
            return key;
    except:
        print("Unable to read from ESP32-S2!")

    keyfile.close();
    removeFile('flash.bin');

# A function that generates a new encryption key using
# the Fernet algorithm and writes it to a file named
# "keyfile.bin". The function then attempts to write
# the key file to an ESP32-S2 microcontroller at memory
# address 0x8000 using the esptool package.
# If the write operation is successful, the function
# returns the contents of the key file. Otherwise,
# an error message is printed. In the end, the transient
# "keyfile.bin" is deleted from the drive so that the
# remaining copy exists only on the ESP32-S2.
def keygen(external):
    if(not external):
        key = Fernet.generate_key();
        with open('keyfile.bin', 'wb') as keyfile:
            keyfile.write(key);
        print("keyfile.bin created.");

    else:
        key = Fernet.generate_key();
        with open('keyfile.bin', 'wb') as keyfile:
            keyfile.write(key);
        try:
            args = ['--port', 'COM3', 'write_flash', '0x8000', 'keyfile.bin'];
            esptool.main(args);
            removeFile('keyfile.bin');

            if (not os.path.exists('fc.ini')):
                with open('fc.ini', 'w') as config:
                    config.write("[config]\ninitial = false")

        except:
            print("Unable to write to ESP32-S2!")
            removeFile('keyfile.bin');

# The main function that initializes the program by displaying
# the menu options and prompting the user for input. The function
# sets the initial values of the keyfile variable to False and
# the file variable to "No File Selected". The function loops
# until the user selects the "Quit" option. For each menu option
# selected, the function calls the corresponding function to
# perform the desired action.
def init_menu():
    alive = True;
    ext_method = True;
    externalExists = False;
    localExists = False;
    file = "No File Selected";

    if (os.path.exists('keyfile.bin')):
        localExists = True;

    if (os.path.exists('fc.ini')):
        externalExists = True;
        if(not localExists):
            print("Assuming key is written to ESP32, using External Method for key.");
        else:
            print("Both local and external keys exist. Defaulting to External Method.")



    printMenuOptions();

    while(alive):
        if (os.path.exists('keyfile.bin')):
            localExists = True;

        if (os.path.exists('fc.ini')):
            externalExists = True;
        print("--------------------------------\nCurrent File: %s" % file);
        print("Key written to ESP32-S2: %s" % str(externalExists));
        if(ext_method):
            print("Method: External (Using key stored on ESP32-S2)");
        else:
            print("Method: Local (Using locally stored key)");

        if(not localExists and not externalExists):
            print("[NOTE] It appears this is your first time using this program. Please generate a key before attempting to lock any files.");

        selection = input("Select an option:");
        if (selection.lower() == 'f'):
            file = openFile();

        if(selection.lower() == 'l'):
            lock(file, ext_method);

        elif(selection.lower() == 'u'):
            unlock(file, ext_method);

        elif (selection.lower() == 'h'):
            printMenuOptions();

        elif(selection.lower() == 'k'):
            if(ext_method):
                if(externalExists):
                    overwriteConfirm = confirm("Doing this will overwrite your existing keyfile, making it impossible to unlock files that are currently locked. Are you sure you want to do this?");
                    if(overwriteConfirm == True):
                        keygen(ext_method);
                    else:
                        print("Using exiting key.");
                else:
                    keygen(ext_method);

            else:
                if(localExists):
                    overwriteConfirm = confirm("Doing this will overwrite your existing keyfile, making it impossible to unlock files that are currently locked. Are you sure you want to do this?");
                    if(overwriteConfirm == True):
                        keygen(ext_method);
                    else:
                        print("Using exiting keyfile.");
                else:
                    keygen(ext_method);

        elif (selection.lower() == 'm'):
            ext_method = (not ext_method);
            if (ext_method):
                print("Switching to External Method via ESP32-S2.");
            else:
                print("Using locally stored key.")

        elif(selection.lower() == 'q'):
            alive = False;

        else:
            print("Invalid selection.");

# This is the main function, where everything begins... Including this segue to our sponsor!
if __name__ == '__main__':
    print_hi('Flashcrypt!\n\----------')
    init_menu();